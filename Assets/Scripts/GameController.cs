﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public static GameController Instance;

    [SerializeField]
    private GameObject _aliens;
    [SerializeField]
    private GameObject _player;
    [SerializeField]
    private bool _resetScoreOnStart;
    private int _currentScore;
    private int _highScore;
    private bool _isScoreAdderRunning;

    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        LevelManager.Instance.StartGame();

        if (_resetScoreOnStart)
        {
            PlayerPrefs.SetInt("CurrentScore", 0);
        }
        _currentScore = PlayerPrefs.GetInt("CurrentScore", 10);

        _highScore = PlayerPrefs.GetInt("HighScore", 0);
        _isScoreAdderRunning = false;
    }

    public void TestWin()
    {
       // LevelManager.Instance.AddKillAlien();

        if (AliensGroupController.Instance.AllMobsKill()) // (LevelManager.Instance.GetAlien() == 20)
        {
            if (!_isScoreAdderRunning)
            {
                StartCoroutine(AddScoreOverTime(2000));
            }

            if (LevelManager.Instance.NextOrEnd()) // проверка непрошли ли игру
            {
                StartCoroutine(NextLevel());
            }
            else
            {
                StartCoroutine(WinGame());
            }            
        }
        if (_aliens.transform.position.y <= -5)
        {
            _player.SetActive(false);
        }
    }

    public void Lose()
    {
        StartCoroutine(LoseScene());
    }

    public void RefreshHighScore()
    {
        if (_currentScore > _highScore)
        {
            PlayerPrefs.SetInt("HighScore", _currentScore);
            _highScore = _currentScore;
            CanvasTextController.Instance.RefreshScore(_currentScore, _highScore);
        }
    }

    public void RefreshCurScore(int value)
    {
        _currentScore += value;
        //PlayerPrefs.SetInt("CurrentScore", _currentScore);
        CanvasTextController.Instance.RefreshScore(_currentScore, _highScore);
    }

    IEnumerator AddScoreOverTime(int value)
    {
        _isScoreAdderRunning = true;
        int addedScore = 0;
        while (addedScore < value)
        {
            RefreshCurScore(100);
            addedScore += 100;
            yield return new WaitForSeconds(0.05f);
        }
    }

    //IEnumerator ReloadScene(int seconds)
    //{
    //    yield return new WaitForSeconds(seconds);
    //    Scene scene = SceneManager.GetActiveScene();
    //    SceneManager.LoadScene(scene.name);
    //}

    IEnumerator NextLevel()
    {
        CanvasTextController.Instance.Win();
        yield return new WaitForSeconds(6f);
        CanvasTextController.Instance.HideWinGame();

        LevelManager.Instance.NextLevel();
    }

    IEnumerator LoseScene()
    {
        CanvasTextController.Instance.Lose();
        yield return new WaitForSeconds(4f);
        CanvasTextController.Instance.HideWinGame();

        LevelManager.Instance.StartGame();
    }

    IEnumerator WinGame()
    {
        CanvasTextController.Instance.WinGame();
        yield return new WaitForSeconds(4f);
        CanvasTextController.Instance.HideWinGame();

        LevelManager.Instance.StartGame();
    }
}
