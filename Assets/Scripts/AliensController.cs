﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AliensController : MonoBehaviour
{
    public static AliensController Instance;

    [SerializeField]
    private float _speed = 0.5f;
    [SerializeField]
    private float _wait = 0.4f;
    [SerializeField]
    private float _bound_l = 9;
    [SerializeField]
    private float _bound_r = 9;
    [SerializeField]
    private float _speedMultiplier = 0.01f;
    [SerializeField]
    private float _chanceOfAttack = 0.02f;

    private bool _invert = false;

    [SerializeField]
    private GameObject _bullet;
    [SerializeField]
    private GameController _gameController;

    [SerializeField]
    private Transform _transformObj;

    private void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        InvokeRepeating("AliensAttack", 0, _wait);
    }

    void AliensAttack()
    {
        if (_invert)
        {
             _speed = -_speed;
            _transformObj.position += Vector3.down * Mathf.Abs(_speed);
            _invert = false;
            return;
        }
        else
        {
            _transformObj.position += Vector3.right * _speed;
        }

        foreach (Transform alien in _transformObj)
        {
            if (alien.position.x < -_bound_l || alien.position.x > _bound_r)
            {
                _invert = true;
                break;
            }
            if (Random.value < _chanceOfAttack)
            {
                if (!AliensGroupController.Instance.AllMobsKill())
                {
                    Instantiate(_bullet, AliensGroupController.Instance.GetOneAlein().transform.position, alien.rotation);
                    // Instantiate(_bullet, alien.position, alien.rotation);
                }
            }
        }
    }

    public void AlienDied(int value)
    {
        _gameController.RefreshCurScore(value);
        if (_speed > 0)
        {
            _speed = _speed + _speedMultiplier;
        }
        else
        {
            _speed = _speed - _speedMultiplier;
        }
    }
}
