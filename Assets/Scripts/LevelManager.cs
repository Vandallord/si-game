﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public static LevelManager Instance;

    [SerializeField]
    private ImageMobs _imageMobs;

    [SerializeField]
    private int _level;
    [SerializeField]
    private int _miniLevel;

    [SerializeField]
    private GameObject _aliens;

    [SerializeField]
    private int _killsAlen;

    [SerializeField]
    private GameObject _player;

    private Vector2 _aliensTrasform = new Vector2(0, 1.12f);

    void Awake()
    {
        Instance = this;
    }

    public void StartGame()
    {
        _level = 0;
        _miniLevel = 0;

        LoadLevel();
        RespPlayer();
    }

    public void NextLevel()
    {
        _miniLevel++;

        if (_miniLevel > 3)
        {
            _level++;
            _miniLevel = 0;
        }

        LoadLevel();
    }

    private void RespPlayer()
    {
        _player.SetActive(true);
        _player.GetComponent<PlayerController>().RefreshPlayer();
    }

    private void LoadLevel()
    {
    //    NullAlien();
        _imageMobs.StartLevel(_level, _miniLevel);
        CanvasTextController.Instance.LevelShow(_level, _miniLevel);

        for (int i = 0; i < _aliens.transform.childCount; i++)
        {
            _aliens.transform.GetChild(i).gameObject.SetActive(true);
        }

        _aliens.transform.position = _aliensTrasform;
    }

    //public void AddKillAlien()
    //{
    //    _killsAlen++;
    //}

    //public int GetAlien()
    //{
    //    return _killsAlen;
    //}

    //public void NullAlien()
    //{
    //    _killsAlen = 0;
    //}

    public bool NextOrEnd() 
    {
        return _imageMobs.NextOrEnd(_level);
    }
}
