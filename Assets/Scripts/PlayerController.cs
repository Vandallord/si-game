﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    private float _xInput;
    private float _yInput;
    [SerializeField]
    private float _bound;

    [SerializeField]
    private GameObject _bullet;
    [SerializeField]
    private GameObject _playerLifeText;

    private Rigidbody2D _rigidbody2D;
    [SerializeField]
    private float _speed;
    [SerializeField]
    private float _shootSpeed;

    private int _health;
    public int _maxHealth;

    [SerializeField]
    private Vector2 _respPos;

    private Vector3 _screenSize;

    private void Awake()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _screenSize = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0));

       // RefreshPlayer();

      //  StartCoroutine(ShootingRoutine());
    }

    public void RefreshPlayer()
    {
        transform.position = _respPos;
        _health = _maxHealth;
        CanvasTextController.Instance.HealthUpdate(_health, _maxHealth);

        StartCoroutine(ShootingRoutine());
    }

    private void Update()
    {
        _xInput = Input.GetAxis("Horizontal");

        _rigidbody2D.velocity = new Vector2(_xInput, 0).normalized * _speed;

        if (Input.GetAxis("Mouse X") != 0)
        {
            Vector3 worldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            transform.position = new Vector3(worldPosition.x, transform.position.y, 0);
        }
        if (transform.position.x > _screenSize.x - _bound)
        {
            transform.position = new Vector3(_screenSize.x - _bound, transform.position.y, 0);
        }
        if (transform.position.x < -_screenSize.x + _bound)
        {
            transform.position = new Vector3(-_screenSize.x + _bound, transform.position.y, 0);
        }
    }

    private IEnumerator ShootingRoutine()
    {
        while (true)
        {
            if (Input.GetButton("Fire1") ^ Input.GetMouseButtonDown(0) ^ Input.GetKeyDown("space"))
            {
                SFXController.PlaySound("laser");
                Instantiate(_bullet, gameObject.transform.position, Quaternion.identity);
                yield return new WaitForSeconds(1f / _shootSpeed);
            }
            yield return new WaitForSeconds(0);
        }
    }

    public void TakeHit(int dmg)
    {
        _health -= dmg;

        CanvasTextController.Instance.HealthUpdate(_health, _maxHealth);

        if (_health <= 0)
        {
            GameController.Instance.Lose();
            gameObject.SetActive(false);
        }
    }
}
