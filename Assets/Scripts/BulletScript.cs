﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour
{
    [SerializeField]
    private float _speed = 5.0f;

    void Update()
    {
        gameObject.transform.position += Vector3.up * _speed * Time.deltaTime;
    }

    void OnBecameInvisible()
    {
        Destroy(gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Enemy")
        {
            collision.gameObject.GetComponent<EnemyController>().TakeHit();
            Destroy(gameObject);
        }
      else  if (collision.tag == "Base")
        {
            collision.gameObject.GetComponent<BaseController>().TakeHit();
            Destroy(gameObject);
        }

        //if (collision.gameObject.GetComponent<BaseController>())
        //{
        //    collision.gameObject.GetComponent<BaseController>().TakeHit();
        //    Destroy(gameObject);
        //}
    }
}
