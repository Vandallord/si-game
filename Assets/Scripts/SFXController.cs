﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFXController : MonoBehaviour
{
    public static AudioClip LaserSound, ExplosionSound;
    public static AudioSource AudioSrc;

    void Start()
    {
        LaserSound = Resources.Load<AudioClip>("laser");
        ExplosionSound = Resources.Load<AudioClip>("explosion");

        AudioSrc = GetComponent<AudioSource>();
    }

    public static void PlaySound(string clip)
    {
        switch (clip)
        {
            case "laser":
                AudioSrc.PlayOneShot(LaserSound);
                break;
            case "explosion":
                AudioSrc.PlayOneShot(ExplosionSound);
                break;
        }
    }
}
