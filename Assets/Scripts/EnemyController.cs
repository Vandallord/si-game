﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    [SerializeField]
    private GameObject _explosion;
    [SerializeField]
    private int _scoreValue = 100;
    [SerializeField]
    private int _health = 1;

    public void TakeHit()
    {
        _health -= 1;
        if (_health <= 0)
        {
            SFXController.PlaySound("explosion");
            Instantiate(_explosion, transform.position, transform.rotation);
            AliensController.Instance.AlienDied(_scoreValue);
            GameController.Instance.RefreshHighScore();
            AliensGroupController.Instance.KillAlien(gameObject);
            gameObject.SetActive(false);
            GameController.Instance.TestWin();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Base")
        {
            Destroy(collision.gameObject);
        }
    }
}
